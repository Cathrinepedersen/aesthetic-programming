# MiniX4 Readme

Note: Please open console to see what my program does. 

[See my program](https://cathrinepedersen.gitlab.io/aesthetic-programming/MiniX4/)

[See my code!](https://gitlab.com/Cathrinepedersen/aesthetic-programming/-/blob/master/MiniX4/MiniX4.js)

![](whatdoyousee.png)

**Provide a title for and a short description of your work (1000 characters or less) as if you were going to submit it to the festival.** 

WHAT DO YOU SEE?

This program focuses on what you personally see when you see a picture of a cow. 
What do you see? A living being? Or do you see your next meal? This program is supposed to count each click on either “a living being” and “food” with the aim to collect data from different individuals to set a bigger focus on our ways of living as human beings in a world where we decide everything, from where we live, to what and who we choose to eat. The program is supposed to count each click to collect data that can show what the majority of individuals see. 


**Describe your program and what you have used and learnt.**

My program counts each click on the buttons “a living being” and “food”. This program has a personal and biased output because I care for all living beings and would never eat the meat of an animal. But I also think that this topic is very important to talk about, both because of morality and the topic of climate change. 

Making this I have learnt how to put an image into the program. 

I have also learnt to put in a text and changing the size of it: “what do you see?”. 

Otherwise, I have learnt to create two buttons with the “createButton”, from there I’ve learnt how to change positions of the buttons and how to count each click on each of the two buttons separately. 

I have also learnt that it wasn’t possible to count every click from different individuals even though this was the goal of my program. I wanted to make my program continue to count, likewise the program designed by Winnie Soon: http://siusoon.net/nonsense/. But after watching a ton of videos and searching on Google, I still didn’t know how to do it. After getting help from our instructors, I was told that my goal wasn’t possible in this type of program that we are working with.


**Articulate how your program and thinking address the theme of “capture all.” – What are the cultural implications of data capture?**

My program doesn’t capture much data. But since I made it, it doesn’t allow for other input than what I want. It isn’t possible to create your own answer. Others can only press either one of the buttons that I wanted to create, which makes me think about everything else made that captures data. Programs that capture data are made by someone to someone else. But you never know the intentions behind the creator. Likewise, you never really know the outcome unless your program is tested multiple times, even then, it is impossible to know exactly. 

I think it is scary to think about how much personal data you allow to be captured. A simple thing as your home address. You write it everywhere when you are ordering something, and even your computer remembers your data, so you don’t have to write it in next time you order something. Even when we are asleep there exist devices that capture your data:


> If sleep was once thought to be the last refuge from capitalism where no value could be extracted,  then this no longer seems to be the case. _(Soon & Cox, p. 115)_

It is also scary to think about how big an impact some data has on our society, for example the Facebook “like” button, which has changed a lot about our digital world, but at the same time has gotten to a point that isn’t always healthy for individuals, where likes have become a part of one’s identity. That the most important thing of one’s digital media is to get enough like. This has a big impact on some individuals that compare likes to their likeability as a person.  The Facebook “like” button also shows that one’s intentions may not always be the outcome, because the button was designed to create a positive atmosphere but did the opposite in some degree. 

**References**

Soon Winnie & Cox, Geoff, "Data capture", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 97-119

[p5.js](https://p5js.org/reference/#group-DOM)

[Daniel Shiffman](https://www.youtube.com/watch?v=587qclhguQg)

