var rainbow;
var scl = 20; //scale
var rain;
let img;

function preload(){ //image for the background.
  sky = loadImage('images/sky.jpeg');
}

function setup(){
  createCanvas(500,600);
  rainbow = new Rainbow();
  frameRate(10);
  pickLocation();
}

function pickLocation(){ //here rows and cols is made. This is so that the rainbow knows where to go.
  var cols = floor(width/scl); //floor is used to get to the closest value that is less than or equal ti the value of the parameter(p5.js floor)
  var rows = floor(height/scl);
  rain = createVector((floor(random(cols))), (floor(random(rows))));
  rain.mult(scl);
}

function draw(){
  background(sky);

    if (rainbow.eat(rain)){ //if statement: if the rainbow eats the rain, pick a new location for a new piece of rain
      pickLocation();
    }
  rainbow.death();//calling the death function from 'snake.js'
  rainbow.update();//calling the update function from 'snake.js'
  rainbow.show();//calling the show function from 'snake.js'

  fill('#67A4CC');//color of the food/raindrop
  rect(rain.x, rain.y, scl,scl);
}

function keyPressed(){
  if (keyCode === UP_ARROW){
    rainbow.dir(0, -1);
  } else if (keyCode === DOWN_ARROW){
    rainbow.dir(0,1);
  } else if (keyCode === RIGHT_ARROW){
    rainbow.dir(1,0);
  } else if (keyCode === LEFT_ARROW){
    rainbow.dir(-1,0);
  }
}
