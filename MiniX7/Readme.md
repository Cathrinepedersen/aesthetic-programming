# MiniX7 Readme 

[See my program!](https://cathrinepedersen.gitlab.io/aesthetic-programming/MiniX7)

[See my Code!](https://gitlab.com/Cathrinepedersen/aesthetic-programming/-/blob/master/MiniX7/minix7.js)

![](miniX7.png)

# The Rainbow Game
 
For this MiniX I found it very difficult to start a completely new game. Therefore, I have made the Snake Game, from Daniel Schiffman’s video: Coding Challenge 3: The Snake Game
 
 
I followed along in Atom during the video and tried to understand each of the syntaxes and methods he used to understand his code and game well. I found that doing the MiniX this way, have helped me to gain a better understanding of methods and objects that we have focused on for this week. 

 
I changed a few things in the game to make it a little bit different. Therefore, it is not a Snake Game, but a Rainbow Game.

 
The game works like the original Snake Game. You use the arrow keys to control, in this case,  the rainbow, to catch the rain that appears around the canvas, to make the rainbow bigger.

 
In constructing the game, two seperate js files were used. The first one is to make the basics of the game, and another one is to create the objects and their behavior. The two js files are called: 

 
Minix7. js:

- Function setup

- Function pickLocation

- Function draw

- Function keyPressed

So, the basics of the program, and the main control of the game. 

Rainbow.js:

- This.eat = function(pos) --> how the eating happens

- This.death = function --> how the rainbow 'dies'

- This.update = function --> the update of each time something happens

- This.show = function --> how the rainbow looks
 
Here all the objects are stored and created. These functions are called in minix7:

- Rainbow.death();

- Rainbow.update();

- Rainbow.show();

 
Because I found this miniX very difficult, it helped me a lot to follow along the video Daniel Shiffman made, because it made me understand OOP in a different way. OOP are programs made with 'hidden' data. The data is sort of put into boxes, that you can call in your main sketches, which makes the main program more understanding and not so long. It also means that some of the program is hidden. 

 
>"Objects, in object orientation, are groupings of data and the methods that can be executed on that data, or stateful abstractions" (Fuller & Goffey, p. 1)
 

I am still not quite sure what the means of object abstraction is. But how I understand it, is that it is about representations. How something is seen and represented. This is not quiet something I have thought about doing the making of this minix. I think that this aspect might be more important when it comes to OOP's that are showing something more realistic. To give an example, when you want to make an account in a program, and you are asked to choose what your gender is, it is possible that the boxes you can click on is only 'female' and 'male', which is not enough options. Here it is important to think beyond just women and men, because the society consists of multiple genders that also needs an option. 


 
> "Object abstraction in computing is about representation. Certain attributed and relations are abstracted from the real world, whilst simutaneously leaving details and contexts out" (Soon & Cox p. 145)



# References
Soon, Winnie & Cox, Geoff, "Preface", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 12-24

Matthew Fuller and Andrew Goffey, “The Obscure Objects of Object Orientation,” in MatthewFuller, How to be a Geek: Essays on the Culture of Software (Cambridge:Polity, 2017). 

Daniel Shiffman: https://www.youtube.com/watch?v=AaGK-fj-BAM

P5.js references: https://p5js.org/reference/
