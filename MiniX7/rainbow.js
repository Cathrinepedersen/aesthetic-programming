function Rainbow(){
  this.x = 0;
  this.y = 0;
  this.xspeed = 1;
  this.yspeed = 0;
  this.total = 0;
  this.tail = [];

  this.eat = function(pos){ //this makes it possible for the rainbow to eat the rain
    var d = dist(this.x, this.y, pos.x, pos.y);
      if (d < 1){
        this.total++;
        return true;
      }else{
        return false;
      }
    }

  this.dir = function(x, y){ //the function for the x and y
    this.xspeed = x;
    this.yspeed = y;
  }

  this.death = function(){ //here is the function for the death. You die by 'eating' yourself, or run into the borders of the canvas
    for (var i = 0; i < this.tail.length; i++){
      var pos = this.tail[i];
      var d = dist(this.x, this.y, pos.x, pos.y);
      if (d < 1){
        this.total = 0;
        this.tail = [];
      }//the tail goes back to just one rect when you die.
    }
  }

  this.update = function (){//this function contains syntaxes that makes it possible for the game to "update" each time something happens
    if (this.total === this.tail.length){
      for (var i = 0; i < this.tail.length-1; i++){
        this.tail[i] = this.tail[i+1];
      } //here you add length to the tail
    }
    this.tail[this.total-1] = createVector(this.x, this.y);//createVector gives you the opputunity to have both an x and an y

    this.x = this.x + this.xspeed*scl; //x value changes by the xspeed value
    this.y = this.y + this.yspeed*scl;//y value changes by the yspeed value

    this.x = constrain(this.x, 0 ,width-scl);
    this.y = constrain(this.y, 0, height-scl);
  } //constrains a value between a minimum and a maximum value --> //so that the snake stays inside the drawn canvas


  this.show = function(){ //this is the 'rainbow', here is also made so that the tail gets longer in the for loop
    colorMode(HSB);
    stroke(255);
    fill(random(255),255,255);
    for (var i = 0; i < this.tail.length; i++){
      rect(this.tail[i].x, this.tail[i].y, scl, scl);
    }
    rect(this.x, this.y, scl, scl); //placement of the rect
  }
}
