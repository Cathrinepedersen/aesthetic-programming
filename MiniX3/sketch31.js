function setup(){
  createCanvas(windowWidth, windowHeight);
  frameRate(8);
}

function draw(){
  background(255, 15);
  //drawElements();
//}
//function drawElements(){

push();
translate(width/2,height/2);
  rotate(millis()/250);
noStroke();
colorMode(HSB);
fill(random(255),250,200);
ellipse(40,0,22,22);
pop();

  push();
  translate(width/2,height/2);
  	rotate(millis()/100 );
  noStroke();
  fill(random(255),10,400);
  ellipse(80,0,22,22);
  pop();


  push();
  translate(width/2,height/2);
    rotate(millis()/500);
  colorMode(HSB);
  noStroke();
  fill(random(255),20,255);
  ellipse(120,0,22,22);
  pop();
}

function windowResized(){
  resizeCanvas(windowWidth,windowHeight);
}
