# MiniX3 Readme

[See my program](https://cathrinepedersen.gitlab.io/aesthetic-programming/MiniX3) 

[See my code!](https://gitlab.com/Cathrinepedersen/aesthetic-programming/-/blob/master/MiniX3/sketch31.js)

![](MiniX3.png)

This program is made as a modified version of the throbber made in the book: Aesthetic Programming by Soon & Cox, page 75. 

My MiniX3 is a colorful throbber that continues until the browser is updated. If updated the throbber starts over and continues to circulate. The program consists of three independent circles that circulates around one another. The syntaxes that I’ve chosen to focus of is the “push” and “pop” functions that helps me create independent circles with different parameters. I’ve also chosen to make each of the three circles circulate at different speeds, where I used the syntax (random(millis()/250);. I made the throbber this way, because i wanted to make if both similar but also different from the original throbbers. Therefore the throbber I made consists of three different circles, with colorful ellipses, to try and make the throbber a bit more interesting to look at. Because i made the ellipses circulate, was to make the throbber more dynamic. 

Living in a very digital world, I’ve come to notice there are many different kinds of thobbers, which are all trying to tell a user, that the program is “working on it” or “wait a minute”. It lets us know that something is happening, and that we need to wait. 

> The main example for this chapter is the graphical spinning wheel icon of a preloader, or so- called “throbber,” 4 that indicates when a program is performing an action such as downloading or intensive calculations. (Soon & Cox, p. 74)

The trobber is a/can be a very simple icon that easily communicates the processing of a program, instead of having a long text telling us that we need to wait or that something is being updated. I personally think the trobber is a great way to simplify this kind og information. Even though the trobber is gray, and for me a bit boring looking, it is serving its purpose right. It is used everywhere in differnt shapes, but all very similar, and it is something that i think everyone know of. 

**References**

Soon Winnie & Cox, Geoff, "Infinite Loops", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 71-96

[p5.js](https://p5js.org/reference/#/p5/millis)
