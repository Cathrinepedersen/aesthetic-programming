function setup() {
  createCanvas(1500,740);
  background(255);
  colorMode(RGB);
}

function draw() {
frameRate(15);
//stem
  noFill();
  strokeWeight(10);
  stroke('#78C372');
  curve(340, 426, 320, 626, 320, 424, 573, 261);

//flower
  stroke('#BD6822');
  strokeWeight(4);
  fill('#F57A3D');
  ellipse(390,320,60,40);
  ellipse(250,320,60,40);
  ellipse(320,380,40,60);
  ellipse(320,260,40,60);

if(mouseIsPressed){
stroke(random(255),70,255);
  strokeWeight(4);
  fill(random(255),70,255);
  ellipse(390,320,60,40);
  ellipse(250,320,60,40);
  ellipse(320,380,40,60);
  ellipse(320,260,40,60);
}

//main ellipse
  ellipseMode(RADIUS);
  strokeWeight(4);
  stroke('#E0BB55');
  fill('#FFD460');
  ellipse(320,320,55,55);

//Eyes
  stroke(255);
  fill(0);
  ellipse(300,310,10,10);
  ellipse(340,310,10,10);

//mouth
  fill(255);
  arc(320,340,30,20,0,PI);

//flowerpot
  stroke('#A85A36');
  fill('#BF663D');
  rect(270,560,100,90,15,15,30,30);

  stroke('#A85A36');
  fill('#E87C4A');
  rect(260,550,120,20,15,15,30,30);

  //hanging plant
  //flower ellipse
  ellipseMode(RADIUS);
  strokeWeight(4);
  stroke('#6DC255');
  fill('#68BA52');
  ellipse(1125,230,70,55);

  //hanging flowerpot
  stroke('#A85A36');
  fill('#BF663D');
  rect(1050,230,150,80,15,15,100,100);

  stroke('#A85A36');
  fill('#E87C4A');
  rect(1040,220,170,20,15,15,100,100);

  //hanging flowerpot strings
  stroke('#977A6A');
  line(1050,220,1120,75);
  line(1200,220,1120,75);

  //plant strings
    noFill();
    strokeWeight(7);
    stroke('#6DC255');
    curve(1300, 600, 1050, 200, 1040, 424, 873, 461);
    curve(1300, 600, 1090, 200, 1070, 454, 873, 461);
    curve(500, 600, 1160, 200, 1190, 454, 1103, 461);
    curve(500, 600, 1130, 200, 1160, 400, 1103, 461);
    curve(500, 600, 1100, 200, 1110, 500, 1103, 461);

    stroke('#5EA84A');
    strokeWeight(2);
    fill('#6DC255');
    ellipse(1060,200,12,10);
    ellipse(1020,230,12,10);
    ellipse(1025,280,12,10);
    ellipse(1050,250,12,10);
    ellipse(1025,280,12,10);
    ellipse(1050,300,12,10);
    ellipse(1060,350,12,10);
    ellipse(1060,405,12,10);
    ellipse(1035,330,12,10);
    ellipse(1035,380,12,10);

    //second string
    ellipse(1070,460,12,10);
    ellipse(1070,420,12,10);
    ellipse(1095,380,12,10);
    ellipse(1070,330,12,10);
    ellipse(1090,300,12,10);
    ellipse(1090,270,12,10);
    ellipse(1090,220,12,10);

    //third string
    ellipse(1110,200,12,10);
    ellipse(1135,250,12,10);
    ellipse(1160,280,12,10);
    ellipse(1130,320,12,10);
    ellipse(1130,350,12,10);
    ellipse(1140,390,12,10);
    ellipse(1110,420,12,10);
    ellipse(1130,450,12,10);
    ellipse(1120,490,12,10);

    //fourth string
    ellipse(1140,190,12,10);
    ellipse(1180,200,12,10);
    ellipse(1180,250,12,10);
    ellipse(1200,280,12,10);
    ellipse(1190,320,12,10);
    ellipse(1160,370,12,10);

    //fifth string
    ellipse(1220,210,12,10);
    ellipse(1220,210,12,10);
    ellipse(1210,250,12,10);
    ellipse(1235,280,12,10);
    ellipse(1235,320,12,10);
    ellipse(1200,360,12,10);
    ellipse(1220,390,12,10);
    ellipse(1190,450,12,10);
}
