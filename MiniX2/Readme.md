# MiniX2 README


[See my program!](https://cathrinepedersen.gitlab.io/aesthetic-programming/MiniX2)

[See my code!](https://gitlab.com/Cathrinepedersen/aesthetic-programming/-/blob/master/MiniX2/sketch22.js)

![](EmojiSketch.png)

**Describe your program and what you have used and learnt.**


My miniX2 is about emojis, and I’ve decided to make some plant-emojis. I like to use colors and make them match and by using strokes it gives a drawing more dimension. The flower is made out of several ellipses, an arch for the mouth, a curve for the flower stem, and two rectangles for the flowerpot.  I’ve also made a hanging plant with several ellipses as well. 

If mouse is pressed, the flower changes color. 

I’ve learnt to create drawings by using code, and I’ve become more confident in using the different shapes parameters. 

I think that I could have tried to incorporate some movement into my code, but I didn’t feel confident enough to do that this time, even though I’ve made some sketches besides this one with movement. 


**How would you put your emoji into a wider social and cultural context that concerns a politics of representation, identity, race, colonialism, and so on? (Try to think through the assigned reading and your coding process, and then expand that to your own experience and thoughts - this is a difficult task, you may need to spend some time thinking about it).**

I don’t think my code has a big implement onto the concerns of political representation. But I made a flower and a plant because I didn’t want to make something too “human like”, despite the facial expression on the flower. I didn’t want to make a too “human like” emoji because I don’t think those are necessary to implement. I understand that making emojis comes with many questions and concerns of who and what to create so that no one is left out, but I believe that it is a very difficult task to complete. And I don’t really think the human like emojis are that necessary. They are fun and so on, but the task of including everyone’s identity seems like an impossible task. And no one want’s to be left out, which I understand. If the goal of emojis being as natural as possible, I think we have to go back to “ :( , :) “, there is no gender, no race, no identity just expressions, but I don’t think there is any going back to that. 

> We take the example of emoticons — ideograms, typically smileys — as typographic shorthand for expressing facial emotional states such as happiness, “:D”. These have become pervasive in communication, and are no longer simply typographic, but actual pictures which can be funny at times as emojis, but also come with underlying issues related to the politics of representation. (Soon & Cox, p. 53)

**References**

Soon Winnie & Cox, Geoff, "Variable Geometry", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 51-70

[p5.js simple shapes](https://p5js.org/examples/hello-p5-simple-shapes.html)

