# MiniX6 Readme

[See my program!](https://cathrinepedersen.gitlab.io/aesthetic-programming/MiniX6/)

[See my code!](https://gitlab.com/Cathrinepedersen/aesthetic-programming/-/blob/master/MiniX6/minix6.js)

![](minix6.png)


I have chosen to rework my MiniX3 where the assignment was to create a throbber. The throbber I made before was very colorful and had some different dynamics in it. I got some feedback on this work, where it was clear that the one giving me feedback, felt an annoyance towards the original throbber’s indefinite nature. I thought this was a very interesting statement and I can somewhat agree. Therefore, I have chosen to make a new throbber slightly the same, but with a different color scheme and rotation, in order to make the statement from the one giving me feedback, come to live.


The reason why I chose to change the colors of my first MiniX3 to a gray toned throbber is to make awareness of the difference of the two and how just changing the colors can make something like this very much different to look at, and makes the conceptual part of each program different, even though not much has changed. 

 
The one with the colors are very interesting to look at because it is different from most other throbbers that you know of and see on different platforms. The colors make you happy, and the different speed of the circulations also make it more interesting. 

 
To compare with the new throbber, this one is gray and more put together in a way, that the circles circulate right next to each other, and doesn’t give it much air, which can give an annoyance to the throbber. Otherwise, the new throbber circulates in the same speed, which also gives a boring look to it. 

 
I think that this quotation from Soon and Cox, describes very well what the meaning of what aesthetic programming is:

 
> ” Aesthetic programming in this sense is considered as a practice to build things, and make worlds, but also produce immanent critique drawing upon computer science, art, and cultural theory.” (Soon & Cox 2020, page 15)

 
In my program I am trying to criticize the normal way of thinking of a throbber, and making it very clear, that not much is needed to make one program and its outcome very different from another slightly different one. This statement I find very important in the programming language. Both when it comes to art but also science. But programming and aesthetic programming gives you the opportunity to address a societal problem, that you find important, and program something to raise awareness of. You can also use programming as a form of prototyping in a design process. 


Programming is very important in the digital culture, because every program you use, every digital device you own, consist of some type of code. Therefore, the relation between programming and digital culture is very relevant to discuss. Not many are aware of the influences put upon users of digital products, it is somewhat impossible to see what the meaning behind products and programs is, you cannot be sure of how this impacts people. 
The relation is also important in the perspective of critical programming, that sometimes makes society problems visible. Making a critical program makes users think further about other programs that may or may not consist of some of the same critical perspectives.

  
> "Ultimately, critical making is about turning that relationship between technology and society from a “matter of fact” into a “matter of concern” (Ratto & Hertz 2019, page 20)

 
This quote points out that not only problem-solving and computational skills that are important. But that it is also very important to understand the conceptual thinking behind problems and already existing programs, to reflect and draw questions towards whatever program you might sit in front of. 


# References

Soon Winnie & Cox, Geoff, "Preface", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 12-24

Ratto, Matt & Hertz, Garnet, “Critical Making and Interdisciplinary Learning: Making as a Bridge between Art, Science, Engineering and Social Interventions” In Bogers, Loes, and Letizia Chiappini, eds. The Critical Makers Reader: (Un)Learning Technology. the Institute of Network Cultures, Amsterdam, 2019, pp. 17-28
