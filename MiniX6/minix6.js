function setup(){
  createCanvas(windowWidth, windowHeight);
  frameRate(8);
}

function draw(){
  background(255, 15);
  //drawElements();
//}
//function drawElements(){

push();
translate(width/2,height/2);
  rotate(millis()/300);
noStroke();
colorMode(HSB);
fill(random(255),255,0,80);
ellipse(30,0,22,22);
pop();

  push();
  translate(width/2,height/2);
  	rotate(millis()/300);
  noStroke();
  colorMode(HSB);
  fill(random(255),255,0,80);
  ellipse(45,0,24,24);
  pop();


  push();
  translate(width/2,height/2);
    rotate(millis()/300);
  colorMode(HSB);
  noStroke();
  fill(random(255),255,0,80);
  ellipse(60,0,28,28);
  pop();
}

function windowResized(){
  resizeCanvas(windowWidth,windowHeight);
}
