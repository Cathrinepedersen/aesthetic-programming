# MiniX1 Readme

[See my program!](https://cathrinepedersen.gitlab.io/aesthetic-programming/MiniX1/)

[See my code!](https://gitlab.com/Cathrinepedersen/aesthetic-programming/-/blob/master/MiniX1/sketch1.js)

![](MiniX1.png)


**What have you produced?**

In my first assignment I have produced eleven squares. Two lager squares and nine smaller ones. I've decided that the larger squares should have some sort of color to them and found it fun to make them change color. After the two larger squares I wanted to make more, to find out how the parameters work. Therefore, I made another nine squares and made all of them smaller and in a grey color, to make them stand out from the colored ones. 
 
**How would you describe your first independent coding experience (in relation to thinking, reading, copying, modifying, writing code, and so on)?**

 think my first experience have been fun. I started out with a different sketch that I copied from elsewhere and from there modified the different codes written. I found that very fun, because the program would produce something more interesting, than what I was capable of doing. Some of the copied code I could understand, but some of it was also complicated. Therefore, I wanted to start over and try out some basic forms, and really understand how this specific form works, and how to change the color, to get a better understanding before diving into something more advanced. Sometimes it was frustrating if I couldn't get the code to work, but on the other hand very pleasing when it then did.
It is just very important to type the right letters, numbers and signs in order for the code to work.
 
 
**How is the coding process different from, or similar to, reading and writing text?**

Coding is different because of all the signs and numbers you need to type in. You cannot just write "square" and expect a square will appear. But writing code also implements words that we understand from our daily lives, such as "square" and therefore makes it easier to understand. 
 
 
**What does code and programming mean to you, and how does the assigned reading help you to further reflect on these terms?**

Code and programming to me, means that I can learn to be creative in a different way than usually. And getting to learn a new kind of language is very fascinating and being able to understand more of what’s behind the interfaces that we use. 

> It is not simply a new way of reading and writing, but also a new way of thinking and understanding other codes. (Soon & Cox, p. 29)


**References**

Soon Winnie & Cox, Geoff, "Getting started", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 27-48

[Daniel Shiffman](https://www.youtube.com/watch?v=yPWkPOfnGsw&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA&index=3)

[p5.js get started](https://p5js.org/get-started/)

[p5.js reference](https://p5js.org/reference/)

