# MiniX8 Readme


# Text me...
_By Line Marie Dissing Nielsen & Cathrine Pedersen_

[See our program!](https://cathrinepedersen.gitlab.io/aesthetic-programming/MiniX8)

[See our js. file](https://gitlab.com/Cathrinepedersen/aesthetic-programming/-/blob/master/MiniX8/miniX8.js)

[JSON facts](https://gitlab.com/Cathrinepedersen/aesthetic-programming/-/blob/master/MiniX8/facts.json)

[JSON text](https://gitlab.com/Cathrinepedersen/aesthetic-programming/-/blob/master/MiniX8/text.json)

![](textme.GIF)

The program is made out of interest in the newest SoMe tendency, where women all over the world are sharing their stories and worries regarding their experience of going outside alone during the day or night. Multiple women all over the world are showing their support and speaking out about this issue in every social media platform by sharing the image of a sent text message that says: "text me when you get home"(see picture). 

This new speak out comes from a recent murder on a 33 year old woman from London. This woman was on her way home from a friend's place, but she never got home. She went missing and was later found dead. A policeofficer was since then arrested for kidnapping and murder. This incident made women speak out about their experiences with walking home alone, and this program tries to illustrate how important this topic is by showing different comments from women expressed on social media. 

In the program there is one collon next to each side of the picture in the middle. One of the collons shows different statements from Danish women and the other shows facts regarding women being assaulted in Denmark. This program shows that WOMEN FEEL UNSAFE and the world needs to know that it is not okay.
 
The program works like an artwork, where you do nothing but look at it and think about what you see. There are different syntaxes used, but the main description of how the program works is that there are two separate JSON files; one with statements from different women, and one with different facts about women being assaulted. In these files we have only chosen a few out of all the statements and facts we could find, even though there are many others to find. But by only choosing a few, and making the statements and facts appear on top of each other, it still visually gives the outcome of a lot of more statements and facts. The way that the statements and facts appear on top of each other and in the end it is just a black space, illustrates how many women feel unsafe, compared to if we had made the text disappear each time.
 
In this program we have learnt to work with JSON files, where you can put in data and use the data in the js file and not have your code to be too long and confusing. In each JSON file we have made an array which contains all the statements and facts. By doing so we have learnt how a JSON file is different from a js file in how you have to write.
 
 
In our js. File we learnt how to load the JSON file by doing so:
 
```
function preload(){
imageText = loadImage("textme.jpeg");
textWomen = loadJSON("text.json");
factWomen = loadJSON(“facts.json”);
```


In the beginning of the code, to ensure the files are uploaded before the program is run. 

We have also learnt how to make the text appear by using a for loop:

```
for (let i = 0; i < 5; i++){
  textSize(random(5,15));
  fill(10,20,40,90);
  textAlign(LEFT);
  textFont("American Typewriter");
  text(textWomen[i].myStatement, 10, random(windowHeight));
}
```


```
for (let j = 0; j < 6; j++){
  textSize(random(5,15));
  fill(10,20,40,90);
  textAlign(RIGHT);
  textFont("American Typewriter");
  text(factWomen[j].assaultFacts, 1190, random(windowHeight));
}
```


We have used a for  loop on each JSON-file to make the text from the files run repeatedly and make  the text appear on top of each other - the quotes from  women on the left side of the image and the facts about assulted women in Denmark on the right side of the image. 

Finally, in this program we  have both learnt how to change  the text in the code in other ways than just changing the font size. The new syntaxes we have used is: textAlign( );  and textFont( ); 
 
The program is generating different comments from women around the world as well as facts about assaults on women. 

> “Although programming languages are clearly not spoken as such, they express particular qualities that come close to speech and even extend our understanding of speech”. (Cox & McLean, p 37)

The way the program keeps generating new text from real people and overlaps each other, made by the for loop, makes the program and specifically the appearance of the text somewhat poetic. The fact that every comment comes from women's experiences also gives the program a poetic feel, because of the way we understand speaking as a form of poetry and in this case the speaking is just not said out loud but made by code and text that in the program makes a loud appearance. 

> “That speech comes from living human bodies further reminds us that coding practices have bodies too, and that coding can only be understood in terms of wider infrastructures, and the context of its making (or “poiesis” if you will). (Soon & Cox, p 167)

By making such a program about this kind of topic, it is important to understand the consequences that come along. This is a program about society and creating further awareness of the problem we try to show, even though it can be seen from many different perspectives. By interfering with tabu topics as a consequence of doing this, there are going to be a lot of different opinions, both good and bad ones. 

> “Indeed, saying words or running code or simply understanding how they work is not enough in itself. What is important is the relation to the consequences of that action, and that is why the analogy of speech works especially well - although ultimately the phrase “speech act” may not be sufficient for the purpose.” (Cox & McLean, p 38)


# References

Soon Winnie & Cox, Geoff, "Vocable Code", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 165-186		

Geoff Cox and Alex McLean, “Vocable Code,” in Speaking Code (Cambridge, MA: MIT Press,2013), 17-38.

Statement from different Danish Women: “Altfordamerne” instagram post on 15th of march 2021

Facts regarding assaults and fear of assault among Danish women: “Altfordamerne” instagram post on 16th of march 2021 (their source: “Megafon for TV2”). 

[JSON formatter](https://jsonformatter.curiousconcept.com/)

[Text and type](https://creative-coding.decontextualize.com/text-and-type/)

Daniel Shiffmann:

[What is JSON part 1](https://www.youtube.com/watch?v=_NFkzw6oFtQ&list=PLRqwX-V7Uu6a-SQiI4RtIwuOrLJGnel0r&index=2) 

[What is JSON part 2](https://www.youtube.com/watch?v=118sDpLOClw&list=PLRqwX-V7Uu6a-SQiI4RtIwuOrLJGnel0r&index=3) 
