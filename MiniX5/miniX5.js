let x = 0;
let y = 0;
let spacing = 30;
let gridSpace = 30;

function setup() {
  createCanvas(windowWidth, windowHeight); //canvas settings.
  background(255);
  frameRate(30)
  grid(); //caaling function grid(){}
}

function grid(){ //making the grid, help from langtons ant --> for loop
  for (let x = 0; x <= width; x += gridSpace){
  for (let y = 0; y <= height; y += gridSpace){
    stroke(20,100,200);
      strokeWeight(2);
      fill(255, 30,100,80);
      rect(x,y,gridSpace,gridSpace);//gridspace, made to 20 let gridSpace=20;
    }
}
}

function draw() { //inspiration from 10PRINT
  if (random(1) < 0.5) { //if random function is true draw first rect --> if-statement
    noFill()
    strokeWeight(3);
    stroke(random(255), 30, 200);
    rect(x, y, spacing, spacing);
  } else {
    noStroke();
    fill(random(255),30,200, 70);
    rect(x, y, spacing, spacing);//if random function is false, draw second rect
  }
  x = x + spacing; //if the rectangles becomes bigger than width then start again at x = 0
  if (x > width) {
    x = 0;
    y = y + spacing++; //making the rects bigger.
  }
}
