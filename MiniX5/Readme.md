# MiniX5 Readme

[See my program!](https://cathrinepedersen.gitlab.io/aesthetic-programming/MiniX5)

[See my code!](https://gitlab.com/Cathrinepedersen/aesthetic-programming/-/blob/master/MiniX5/miniX5.js)

![](MiniX5.png)


My program is a combination/inspiration from both the 10PRINT and Langton’s Ant program. I set some rules which is, that I wanted to make rectangles go from one site of the page to another. I wanted these rectangles to be different, but still fit together, therefor I made two different rectangles and put them in an if-statement to make them randomly change from one to the other. 

Otherwise, I made the grid, which comes from Langton’s Ant, to give the output even more dimension and to try making the program some type of artwork. 


The roles my rules have in my work and its process, is that each time you load the program the different colors and strokes are randomly placed, which gives it a different but somewhat alike output each time. 


Making this assignment, made me think a lot about the term pseudorandom where this quote describes it very well: 


> “(...)the significance of “pseudorandomness”—the production of random-like values that may appear at first to be some sad, failed attempt at randomness, but which is useful and even desirable in many cases“ (Montfort et al. p. 120)


I feel like, even though you may try to make a work random, like the 10PRINT and Langton’s Ant, these works are still generated and made by rules which is set by the creator, so to say something in the digital world is completely random is hard to believe. Otherwise, I think that it is very interesting still, that you can tell a computer to do something, and make it choose between random numbers, and make it generate different output each time loaded. 

**References**

Soon Winnie & Cox, Geoff, "Auto-generator", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 121-142

Nick Montfort et al. “Randomness,” 10 PRINT CHR$(205.5+RND(1)); : GOTO10, https://10print.org/ (Cambridge, MA: MIT Press, 2012), 119-146.

[Daniel Shiffman](https://www.youtube.com/watch?v=OTNpiLUSiB4)

[Daniel Shiffman 10 PRINT](https://www.youtube.com/watch?v=bEyTZ5ZZxZs)
