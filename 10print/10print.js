let x = 0;
let y = 0;
let spacing = 30;

function setup(){
  createCanvas(windowWidth,windowHeight);
  background(50);
}

function draw(){
  stroke(random(255),255,0);
  if (random(10) <8){
    line(x,y,x+spacing,y+spacing);
  } else {
    line(x,y+spacing,x+spacing,y);
  }
  x+=10;
  if (x>width){
    x=0;
    y+=spacing;
  }
}
