# MiniX10 Readme

![](Datacapture.png) ![](OOP.png)

**What are the difficulties involved in trying to keep things simple at the communications level whilst maintaining complexity at the algorithmic procedural level?**

Creating a flowchart is a method to elaborate your program and clarify the practical and conceptual aspects of the algorithm within.

> “Flowcharts, “Flow of the chart →  chart of the flow”, have been considered a fundamental explanatory tool since the early days of computer programming. One of their common uses is to illustrate computational operations and data processing for programming by “converting the numerical method into a series of steps.”” (Soon & Cox 2020 p. 214)

The difficulties involved within creating a flowchart and keeping things simple at the commutations level whilst maintaining the complexity is to translate everything from a coding language to the spoken language. It is almost like translating a German text in high school for the first time. 

> “The diagram is an “image of thought,” in which thinking does not consist of problem-solving but — on the contrary — problem-posing.” (Soon and Cox 2020, p. 221)  
 
Individual: Having to make something complex and in a different "language" and trying to simplify it to some sort of readable text form is difficult at first, but after trying it a couple of times, it makes a lot of sense to why it is an important tool to know. The flowchart is giving a draft of the idea we might have, without at first having to think to much about which syntaxes precisely is needed, therefore it can be easier to work on a program, if you have made a flowchart first. 


**What are the technical challenges facing the two ideas and how are you going to address these?**

Making the flowcharts and generating different ideas for this week's MiniX, were very different from just making the program to begin with. During the making of each flowchart, we were talking about how we were going to make each program, and which syntaxes we were going to use. We weren't quite sure of how to actually make each program but were looking around trying to figure out if the programs were even possible for us to make. We found some syntaxes, and some games that had some similarities to what we wanted, and from there we made the flowcharts knowing that the programs are possible to make, even though we are not sure yet, how to technically make them. 

Individual: First of all, in the game we want to make, we need to know how to make the program work, and how we actually want the game to execute, we have a draft, but the idea is not final. 

In the other idea about data capture, the technical difficulties are that we want to be able to take pictures of the person using the program. This is something we have not tried yet, but we have found a page that tells us how to do it. 

**In which ways are the individual and the group flowcharts you produced useful?**

The flowcharts can be useful as a tool to communicate how you envision your idea. When making the group flowchart we also found that the flowcharts helped make clear what technical knowhow we need(ed) to create the programs we envision. While making clear what difficulties we would have technically, the flowchart also made clear how our ideas maybe weren’t as complicated as we first imagined. The flowcharts allowed us to quickly get an overview of our ideas and see if the ideas were possible for us.

Individual: The two flowcharts we made in the group is useful to making our final project, because we now have an idea and a draft of it. It is useful to know where to beging when it comes to making the code, and by making the flowchart at first, it makes it easier to start. Making the flowchart first dosen't have to mean that you need to go into details, as much as when you have to make the actual program, this, i think, leads to more interesting outcomes. 
> ”As we discussed in previous chapters, programming is a form of abstraction that requires the selection of important details in which the implementation embodies the programmers’ thinking and decision-making processes.” (Soon and Cox 2020, p. 222)

# Individual flowchart 
For the individual assignment I have chosen to make a flowchart of my miniX7, which was to create a game. This assignment I thought was the most difficult, even though my program is inspired by Daniel Shiffman. The program had a lot of different syntaxes, and even though I think that I understand the code, it was a different kind of understanding you get when you make a flowchart over a program that is already made. It made me understand it even more than to begin with. 
[See the program here](https://cathrinepedersen.gitlab.io/aesthetic-programming/MiniX7)

![](Flowchartgame.png)

# References

[Daniel Shiffman](https://www.youtube.com/watch?v=AaGK-fj-BAM)

Soon Winnie & Cox, Geoff, "Algorithmic procedures", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 211-226
